public class Patton extends Tank implements Strength, Nation{
    public Patton(String model, String nation, Long serialNumber, Integer weight, Integer tankGunDamage, Integer tankSpeed) {
        super(model, nation, serialNumber, weight, tankGunDamage, tankSpeed);
    }

    @Override
    public void tankStrength() {
        System.out.println("Мощь танка равна = " + (getWeight() + getTankGunDamage() + getTankSpeed()));
    }

    @Override
    public void tankNation() {
        System.out.println(getModel() + " нация этого танка " + getNation());
    }
}
