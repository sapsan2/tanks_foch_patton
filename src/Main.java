public class Main {
    public static void main(String[] args) {
        Foch tankOne = new Foch("Foch155","France",1L,50,2250,60);
        Patton tankTwo = new Patton("T57heavy","USA",2L,60,1600,35);

        tankOne.tankAttack();
        tankOne.tankSpeed();
        tankOne.tankStrength();
        tankOne.tankNation();

        System.out.println();

        tankTwo.tankAttack();
        tankTwo.tankSpeed();
        tankTwo.tankStrength();
        tankTwo.tankNation();
    }
}
