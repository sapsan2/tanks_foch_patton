public abstract class Tank {
    private String model;
    private String nation;
    private Long serialNumber;
    private Integer weight;
    private Integer tankGunDamage;
    private Integer tankSpeed;

    public void tankAttack() {
        System.out.println(getModel() + " атакует врага!");
    }

    public void tankSpeed() {
        System.out.println(getModel() + " едет со скоростью " + getTankSpeed() + " км/ч");
    }

    public Tank(String model, String nation, Long serialNumber, Integer weight, Integer tankGunDamage, Integer tankSpeed) {
        this.model = model;
        this.nation = nation;
        this.serialNumber = serialNumber;
        this.weight = weight;
        this.tankGunDamage = tankGunDamage;
        this.tankSpeed = tankSpeed;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public Long getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(Long serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getTankGunDamage() {
        return tankGunDamage;
    }

    public void setTankGunDamage(Integer tankGunDamage) {
        this.tankGunDamage = tankGunDamage;
    }

    public Integer getTankSpeed() {
        return tankSpeed;
    }

    public void setTankSpeed(Integer tankSpeed) {
        this.tankSpeed = tankSpeed;
    }
}
